import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SearchBlockComponent } from './components/search-block/search-block.component';
import { TopBlockComponent } from './components/top-block/top-block.component';
import { ResultBlockComponent } from './components/result-block/result-block.component';

const routes: Routes = [
  { path: 'top',        component: TopBlockComponent    },
  { path: 'search',     component: SearchBlockComponent },
  { path: 'result',     component: ResultBlockComponent },
];

@NgModule({
imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
