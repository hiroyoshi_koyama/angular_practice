import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SearchBlockComponent } from './components/search-block/search-block.component';
import { TopBlockComponent } from './components/top-block/top-block.component';
import { AppRoutingModule } from './app-routing.module';
import { ResultBlockComponent } from './components/result-block/result-block.component';


@NgModule({
  declarations: [
    AppComponent,
    SearchBlockComponent,
    TopBlockComponent,
    ResultBlockComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
